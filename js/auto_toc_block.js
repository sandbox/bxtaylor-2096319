(function($){

  Drupal.autoTableOfContents = Drupal.autoTableOfContents || {};

  Drupal.behaviors.autoTableOfContents = {
    attach: function (context, settings) {

      settings.auto_toc_selectors =  $.extend({
        basePath: settings.basePath,
        pathToSelf: context.location.pathname
      }, settings.auto_toc_selectors || {});

      var targetUL = document.getElementById('auto-table-of-contents');
      var $outerSelector = $(settings.auto_toc_selectors.outer).not('.auto-toc-behaviors-processed');
      var $innerSelectorsFound = $outerSelector.find(settings.auto_toc_selectors.inner);
      var fragment = document.createDocumentFragment();

      $innerSelectorsFound.each(function(){
        var $this = $(this);
        $this.attr('id', $this.get(0).innerHTML.toLowerCase().split(' ').join('-'));
        var li = document.createElement('li');
        var a = document.createElement('a');
        a.href = settings.auto_toc_selectors.pathToSelf + '#' + $this.get(0).innerHTML.toLowerCase().split(' ').join('-');
        a.innerHTML = $this.get(0).innerHTML;
        li.appendChild(a);
        fragment.appendChild(li);
      });

      targetUL.appendChild(fragment.cloneNode( true ));
      $outerSelector.addClass('auto-toc-behaviors-processed');
    }
  };

})(jQuery);
