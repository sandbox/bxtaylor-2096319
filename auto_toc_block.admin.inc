<?php

/**
 * Admin Form for Auto Table of Contents (TOC) Block
 *
 */
function auto_toc_block_config_form($form, &$form_state) {
  $form = array();
  $form['auto_toc_description'] = array(
    '#markup' => '<p>Helpful description text will go here.</p>',
  );
  $form['auto_toc_outer_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Outer Selector'),
    '#description' => t('The main target element to examine. The Table of Contents will be generated from the inner selectors found in the outer selector.'),
    '#required' => TRUE,
    '#default_value' => variable_get('auto_toc_outer_selector', DEFAULT_OUTER_SELECTOR),
  );
  $form['auto_toc_inner_selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Inner Selector(s)'),
    '#description' => t('A comma-separated list of elements used to generate the table of contents.'),
    '#required' => TRUE,
    '#default_value' => variable_get('auto_toc_inner_selector', DEFAULT_INNER_SELECTOR),
  );
  $form['auto_toc_method'] = array(
    '#type' => 'select',
    '#title' => t('Method of Implementation'),
    '#options' => array(
      'javascript' => t('JavaScript'),
      'php' => t('PHP'),
    ),
  );

  return system_settings_form($form);
}
